﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioInspectEvent : MonoBehaviour
{
    public AudioSource SoundSource;
    public AudioSource PlayerSource;
    public AudioClip KillerClip;
    public AudioClip PlayerClip;
    public SubtitleManager Subtitle;
    Inspectable inspectable;
    bool done;

    private void Start()
    {

        //bool test = false, test2 = false;

        //while(test ? test2 : false)
        //{

        //}

        inspectable = GetComponent<Inspectable>();
        inspectable.AdditionalCommand += () =>
        {
            if (!done)
            {
                SoundSource.clip = KillerClip;
                //Subtitle.EnterSubtitle("I see you found my trophies... All of them failed their test, they couldn't handle the pressure. Tik tok! time's running out.", KillerClip);
                SoundSource.Play();
                if (PlayerClip)
                    PlayerSource.GetComponent<PlayerThoughts>().AudioEvent(PlayerClip);
                done = true;
            }
        };
    }
}
