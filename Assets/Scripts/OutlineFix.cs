﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutlineFix : MonoBehaviour
{
    public List<Inspectable> OutlineObjects = new List<Inspectable>();
    Inspectable inspectable;
    // Use this for initialization
    void Start()
    {
        inspectable = GetComponent<Inspectable>();
        inspectable.AdditionalCommand += () => 
        {
            StartCoroutine(Init());
        };
    }

    // Update is called once per frame
    IEnumerator Init()
    {
        yield return new WaitUntil(() => allPresentHighlight());
        OutlineObjects.ForEach(insp => insp.Highlight(false));
    }

    bool allPresentHighlight()
    {
        int count = 0;
        OutlineObjects.ForEach(o => count += o.outline ? 1 : 0);
        return count >= OutlineObjects.Count;
    }
}
