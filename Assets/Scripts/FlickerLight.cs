﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlickerLight : MonoBehaviour
{
    Light testLight;
    bool flashing = true;
    public float minWaitTime = 0.01f;
    public float maxWaitTime = 0.3f;
    private float timer;
    public AudioClip flickerSound;
    private AudioSource myAudioSource;
    bool isFlickering;
    public bool hasEntered;
    bool checker;

    public Cellphone cellphone;
    public GameObject toEnable;

    // Use this for initialization
    IEnumerator Start()
    {
        myAudioSource = GetComponent<AudioSource>();
        testLight = GetComponent<Light>();

        checker = true;
        yield return new WaitUntil(() => cellphone.Answered);
        toEnable.SetActive(true);
    }

    public IEnumerator Flicker()
    {
        while (checker)
        {
            yield return new WaitForSeconds(Random.Range(minWaitTime, maxWaitTime));
            testLight.enabled = !testLight.enabled;
            hasEntered = true;
        }
    }
}
