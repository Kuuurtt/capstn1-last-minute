﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorEvent : MonoBehaviour
{
    public AudioSource PhoneAudio;
    public Cellphone CellPhone;
    Animator animator;

    IEnumerator Start()
    {
        animator = GetComponent<Animator>();
        animator.SetBool("Closed", true);
        yield return new WaitUntil(() => CellPhone.Answered);
        animator.SetBool("Closed", false); 
    }
}
