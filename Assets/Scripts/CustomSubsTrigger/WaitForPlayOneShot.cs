﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitForPlayOneShot : MonoBehaviour
{
    public AudioSource SoundSource;
    public SubtitleHandler SubtitleHandler;
    public int SubsIndex;
    // Use this for initialization
    IEnumerator Start()
    {
        yield return new WaitUntil(() => SoundSource.isPlaying);
        SubtitleHandler.RemoveSubs();
        SubtitleHandler.AddSubs(SubsIndex);
        Destroy(this);
    }
}
