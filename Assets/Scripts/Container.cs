﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Container : MonoBehaviour
{
    public Inventory PlayerInventory;
    public ItemNotification ItemNotif;
    public List<InventoryItem> ItemsToGive;
    public GameObject CustomCanvas;

    Inspectable inspectable;

    // Use this for initialization
    void Start()
    {
        inspectable = GetComponent<Inspectable>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void GiveItems()
    {
        StartCoroutine(giveItems());
    }

    IEnumerator giveItems()
    {

        foreach (InventoryItem item in ItemsToGive)
        {
            item.gameObject.SetActive(true);
            ItemNotif.Name.text = item.Name;
            ItemNotif.Description.text = item.Description;
            ItemNotif.Icon.sprite = item.Icon.sprite;
            ItemNotif.gameObject.SetActive(true);
            PlayerInventory.AddItem(item, 1);
            item.gameObject.SetActive(false);
            yield return new WaitUntil(() => !ItemNotif.gameObject.activeInHierarchy);
        }
        Destroy(this);
    }
}
