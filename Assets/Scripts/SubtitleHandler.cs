﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubtitleHandler : MonoBehaviour
{
    public List<GameObject> SubtitleTemplates = new List<GameObject>();
    public GameObject SubsLocation;
    GameObject presentSubs;

    void enableSubs(GameObject subs)
    {
        presentSubs = Instantiate(subs, SubsLocation.transform);
        presentSubs.GetComponent<RectTransform>().localPosition = Vector3.zero;
        presentSubs.GetComponent<RectTransform>().localScale = Vector3.one;
        if (presentSubs)
            presentSubs.SetActive(true);
    }


    public GameObject GetCurrentSubs()
    {
        return presentSubs;
    }

    public void AddSubs(int index)
    {
        RemoveSubs();
        if (SubtitleTemplates[index])
        {
            enableSubs(SubtitleTemplates[index]);
        }
    }

    public void RemoveSubs()
    {
        if (presentSubs)
            Destroy(presentSubs);

        presentSubs = null;
    }
}
