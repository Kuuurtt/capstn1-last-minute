﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projector : MonoBehaviour
{
    Inspectable inspectable;
    Animator animator;
    // Use this for initialization
    void Start()
    {
        animator = GetComponentInParent<Animator>();
        inspectable = GetComponent<Inspectable>();
        inspectable.AdditionalCommand += () =>
            {
                inspectable.InspectorUI.gameObject.SetActive(false);
                animator.SetBool("Deployed", !animator.GetBool("Deployed"));
            };
    }
}
