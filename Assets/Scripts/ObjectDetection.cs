﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class ObjectDetection : MonoBehaviour
{
    public float DetectionRange;
    public InspectorPanel InspectorUI;
    public GameObject NotifPanel;
    public Inventory PlayerInventoryUI;
    // [HideInInspector]
    public GameObject CurrentObject;
    Camera parentCam;


    // Use this for initialization
    void Start()
    {
        parentCam = GetComponentInParent<Camera>();
        InspectorUI.Player = GetComponentInParent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        Debug.DrawRay(parentCam.transform.position, transform.forward * DetectionRange, Color.red);
        detect();

        if (CurrentObject)
        {
            if (CurrentObject.GetComponent<Inspectable>().outline.enabled && !InspectorUI.gameObject.activeInHierarchy)
            {
                if (Input.GetKeyDown(KeyCode.E))
                    inspect();
            }
        }

        if (Input.GetKeyDown(KeyCode.Tab) && !InspectorUI.gameObject.activeInHierarchy && !NotifPanel.activeInHierarchy)
            PlayerInventoryUI.gameObject.SetActive(true);
    }

    void detect()
    {
        //int Mask = 1 << 2;
        //Mask = ~Mask;
        RaycastHit rayCastHit;
        if (Physics.Raycast(parentCam.transform.position, transform.forward, out rayCastHit, DetectionRange))//, 2))
            CurrentObject = rayCastHit.collider.gameObject.GetComponent<Inspectable>() ? rayCastHit.collider.gameObject : null;
        else
            CurrentObject = null;
    }

    void inspect()
    {
        if (!CurrentObject.GetComponent<Inspectable>().HighlightOnly)
        {
            GameObject duplicate = Instantiate(CurrentObject.GetComponent<Inspectable>().CloneObject, InspectorUI.ObjectPlacement.position, Quaternion.identity);
            duplicate.layer = 1;
            duplicate.SetActive(true);
            foreach (Transform child in duplicate.transform)
                child.gameObject.layer = 1;
            Inspectable inspectable = CurrentObject.GetComponent<Inspectable>();
            InspectorUI.NameText.text = inspectable.Name;
            InspectorUI.DescriptionText.text = inspectable.Description;
            InspectorUI.CurrentObjectDisplayed = duplicate;
            InspectorUI.gameObject.SetActive(true);
            if (CurrentObject.GetComponent<LockedItem>())
                CurrentObject.GetComponent<LockedItem>().OnInspect();
            if (CurrentObject.GetComponent<Container>())
                CurrentObject.GetComponent<Container>().CustomCanvas.SetActive(true);
            if (CurrentObject.GetComponent<Inspectable>().AdditionalCommand != null)
                CurrentObject.GetComponent<Inspectable>().AdditionalCommand();
        }
    }
}
