﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomCanvas : MonoBehaviour
{
    public InspectorPanel InspectorWindow;

    void Update()
    {
        if (InspectorWindow)
        {
            if (!InspectorWindow.NotifPanel.activeInHierarchy)
            {
                if (Input.GetKeyDown(KeyCode.Space))
                    gameObject.SetActive(false);
            }
        }
    }
}
