﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryItem : MonoBehaviour
{
    public int UniqueID;
    public int StackCount;
    public Text StackText;
    public string Name;
    public Text NameText;
    public string Description;
    public Image Icon;
    [HideInInspector]
    public Inventory InventoryParent;

    private void OnEnable()
    {
        if (!InventoryParent)
            InventoryParent = GetComponentInParent<Inventory>();

        if (InventoryParent)
        {
            if (!InventoryParent.Items.Exists(item => item.UniqueID == UniqueID))
                Destroy(gameObject);
        }
    }

    // Use this for initialization
    void Start()
    {
        InventoryParent = GetComponentInParent<Inventory>();
        NameText.text = Name;
        StackText.text = "x" + StackCount;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ShowItem()
    {
        InventoryParent.InspectItem(this);
    }

    public void UseItem()
    {
        StackCount--;
        StackText.text = "x" + StackCount;
        if (StackCount <= 0)
        {
            InventoryParent.Items.RemoveAt(InventoryParent.Items.FindIndex(item => item.UniqueID == UniqueID));
            InventoryParent.Items.TrimExcess();
            if (InventoryParent.CurrentItem.Equals(this))
                InventoryParent.ClearInspection();
            Destroy(gameObject);
        }
    }
}
