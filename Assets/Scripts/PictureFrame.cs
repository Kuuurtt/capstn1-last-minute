﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PictureFrame : MonoBehaviour
{
    public bool Moving;
    public Transform PlayerParent;
    public ObjectDetection Detector;
    public Camera ViewCam;
    Inspectable inspectable;

    // Use this for initialization
    void Start()
    {
        inspectable = GetComponent<Inspectable>();
        inspectable.AdditionalCommand = () => inspectable.InspectorUI.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Moving)
        {
            Vector3 mousePosition = ViewCam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z + 2f));
            Vector3 clampedTransform = new Vector3(Mathf.Clamp(mousePosition.x, 26.464f, 28.288f), Mathf.Clamp(mousePosition.y, 196.397f, 197.626f), -589.98f);
            transform.parent.position = clampedTransform;
        }

        if (PlayerParent.gameObject.activeInHierarchy)
        {
            if (inspectable.outline)
                inspectable.Highlight(false);
        }
    }


    private void OnMouseOver()
    {
        inspectable.Highlight(true);
    }

    private void OnMouseExit()
    {
        if (!Moving)
            inspectable.Highlight(false);
    }

    private void OnMouseDown()
    {
        Moving = true;
        inspectable.outline.color = 0;
        inspectable.ObjectsToDisable.ForEach(o => o.SetActive(false));
        inspectable.enabled = false;
    }

    private void OnMouseUp()
    {
        Moving = false;
        inspectable.outline.color = 2;
        inspectable.enabled = true;
    }
}
