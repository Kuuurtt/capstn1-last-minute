﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class FocusInspect : MonoBehaviour
{
    public GameObject Player;
    public Camera MainCameraDuplicate;
    public CustomCanvas PersonalCanvas;
    public Transform FocusPoint;
    public float MoveTransitionSpeed;
    public float RotateTransitionSpeed;
    public float FOVTransitionSpeed;
    public float TargetFOV;

    Inspectable inspectable;
    Vector3 originalPosition;
    Quaternion originalRotation;
    float originalFOV;

    // Use this for initialization
    void Start()
    {
        inspectable = GetComponent<Inspectable>();
        inspectable.AdditionalCommand += () =>
        {
            inspectable.InspectorUI.gameObject.SetActive(false);
            originalPosition = MainCameraDuplicate.transform.position;
            originalRotation = MainCameraDuplicate.transform.rotation;
            originalFOV = MainCameraDuplicate.fieldOfView;
            MainCameraDuplicate.transform.parent = null;            
            Player.SetActive(false);
            MainCameraDuplicate.gameObject.SetActive(true);
            StartCoroutine(bringToFocusPoint());
        };
    }

    IEnumerator bringToFocusPoint()
    {
        while ((MainCameraDuplicate.transform.position - FocusPoint.position).magnitude > 0)
        {
            MainCameraDuplicate.transform.position = Vector3.MoveTowards(MainCameraDuplicate.transform.position, FocusPoint.position, MoveTransitionSpeed);
            MainCameraDuplicate.transform.rotation = Quaternion.RotateTowards(MainCameraDuplicate.transform.rotation, FocusPoint.rotation, RotateTransitionSpeed);
            MainCameraDuplicate.fieldOfView = Mathf.MoveTowards(MainCameraDuplicate.fieldOfView, TargetFOV, FOVTransitionSpeed);
            yield return new WaitForFixedUpdate();
        }
        // StartCoroutine(reactivate());
        inspectable.ObjectsToDisable.ForEach(o => o.SetActive(false));
        gameObject.SetActive(false);
        Player.GetComponent<FirstPersonController>().m_MouseLook.DisableLook();
        Player.GetComponent<FirstPersonController>().enabled = false;
        PersonalCanvas.gameObject.SetActive(true);
    }

    IEnumerator reactivate()
    {
        while ((MainCameraDuplicate.transform.position - originalPosition).magnitude > 0)
        {
            MainCameraDuplicate.transform.position = Vector3.MoveTowards(MainCameraDuplicate.transform.position, originalPosition, MoveTransitionSpeed);
            MainCameraDuplicate.transform.rotation = Quaternion.RotateTowards(MainCameraDuplicate.transform.rotation, originalRotation, RotateTransitionSpeed);
            MainCameraDuplicate.fieldOfView = Mathf.MoveTowards(MainCameraDuplicate.fieldOfView, originalFOV, FOVTransitionSpeed);
            yield return new WaitForFixedUpdate();
        }
        MainCameraDuplicate.transform.parent = Player.transform;
        inspectable.ObjectsToDisable.ForEach(o => o.SetActive(true));
        Player.GetComponent<FirstPersonController>().enabled = true;
        Player.GetComponent<FirstPersonController>().m_MouseLook.EnableLook();
        Player.GetComponent<FirstPersonController>().m_MouseLook.InternalLockUpdate();
        Player.SetActive(true);
        MainCameraDuplicate.gameObject.SetActive(false);
    }

    public void BackToPlayer()
    {
        StartCoroutine(reactivate());
    }


}
