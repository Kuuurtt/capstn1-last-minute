﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioTimerEvent : MonoBehaviour
{
    public Timer ClockTimer;
    public AudioSource KillerRadio;
    public List<AudioClip> Clips = new List<AudioClip>();
    public int Minutes;
    public int Seconds;
    public GameObject Player;

    // Use this for initialization
    IEnumerator Start()
    {
        yield return new WaitUntil(() => ClockTimer.Minutes <= Minutes);
        yield return new WaitUntil(() => ClockTimer.Seconds <= Seconds);
        yield return new WaitUntil(() => Player);
        foreach (AudioClip clip in Clips)
        {
            KillerRadio.clip = clip;
            KillerRadio.Play();
            yield return new WaitUntil(() => !KillerRadio.isPlaying);
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<CharacterController>())
            Player = other.gameObject;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<CharacterController>())
            Player = null;
    }
}
