﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class LockedItem : MonoBehaviour
{
    public ItemNotification ItemNotif;
    public GameObject CustomCanvas;
    public Button UnlockButton;
    public int RequiredItemID;
    public List<InventoryItem> ItemsToGive;
    [HideInInspector]
    public Inspectable ObjectInspectable;
    public bool Unlocked;

    // Use this for initialization
    void Start()
    {
        ObjectInspectable = GetComponent<Inspectable>();
        //    Unlocked = false;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnInspect()
    {
        if (CustomCanvas)
            CustomCanvas.SetActive(true);
        if (UnlockButton)
            UnlockButton.interactable = ObjectInspectable.ObjectDetector.PlayerInventoryUI.Items.Exists(item => item.UniqueID == RequiredItemID);
    }

    public void Unlock()
    {
        if (ObjectInspectable.ObjectDetector.PlayerInventoryUI.Items.Exists(item => item.UniqueID == RequiredItemID))
            ObjectInspectable.ObjectDetector.PlayerInventoryUI.Items.RemoveAt(ObjectInspectable.ObjectDetector.PlayerInventoryUI.Items.FindIndex(item => item.UniqueID == RequiredItemID));
        Unlocked = true;
        StartCoroutine(giveItems());
    }

    IEnumerator giveItems()
    {
        foreach (InventoryItem item in ItemsToGive)
        {
            item.gameObject.SetActive(true);
            ItemNotif.Name.text = item.Name;
            ItemNotif.Description.text = item.Description;
            ItemNotif.Icon.sprite = item.Icon.sprite;
            ItemNotif.gameObject.SetActive(true);
            ObjectInspectable.ObjectDetector.PlayerInventoryUI.AddItem(item, 1);
            item.gameObject.SetActive(false);
            yield return new WaitUntil(() => !ItemNotif.gameObject.activeInHierarchy);
        }
    }
}
