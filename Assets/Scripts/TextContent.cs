﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TextContent
{
    public string Content;
    public float TimeStamp;
}
