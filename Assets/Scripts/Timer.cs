﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public Text DisplayText;
    public int Minutes;
    public int Seconds;

    // Use this for initialization
    void Start()
    {
        Minutes = Mathf.Clamp(Minutes, 0, 59);
        Seconds = Mathf.Clamp(Seconds, 0, 59);
        DisplayText.text = Minutes.ToString("00") + ":" + Seconds.ToString("00");
        StartCoroutine(countDown());
    }

    IEnumerator countDown()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f);
            Seconds -= Seconds < 0 ? 0 : 1;
            Minutes -= Seconds < 0 ? 1 : 0;
            Seconds = Seconds < 0 ? 59 : Seconds;
            Minutes = Mathf.Clamp(Minutes, 0, 59);
            Seconds = Mathf.Clamp(Seconds, 0, 59);
            DisplayText.text = Minutes.ToString("00") + ":" + Seconds.ToString("00");
            if (Minutes <= 0 && Seconds <= 0)
                break;
        }
        Debug.Log("Done");
    }
}
