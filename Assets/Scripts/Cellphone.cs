﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cellphone : MonoBehaviour
{
    public AudioSource SoundSource;
    public AudioClip VoiceClip;
    [HideInInspector]
    public bool Answered;
    Inspectable inspectable;

    // Use this for initialization
    IEnumerator Start()
    {
        Answered = false;
        yield return new WaitUntil(() => !GetComponent<cakeslice.Outline>().isActiveAndEnabled);
        inspectable = GetComponent<Inspectable>();
        inspectable.AdditionalCommand += () => 
        {
            inspectable.InspectorUI.gameObject.SetActive(false);
            SoundSource.loop = false;
            SoundSource.clip = VoiceClip;
            SoundSource.Play();
            Answered = true;
            inspectable.ObjectsToDisable.ForEach(o => Destroy(o));
            Destroy(inspectable.outline);
            Destroy(inspectable);
        };
        inspectable.enabled = false;
        Destroy(inspectable.outline);
        yield return new WaitUntil(() => SoundSource.isPlaying);
        inspectable.outline = inspectable.gameObject.AddComponent<cakeslice.Outline>();
        inspectable.outline.color = 1;
        inspectable.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
