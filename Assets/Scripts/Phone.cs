﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Phone : MonoBehaviour
{
    public Text InspectorContinueText;
    public GameObject CustomCanvas;
    public Vault RecycledVaultScript;
    public AudioSource SoundSource;
    public AudioSource CallReceiver;

    public AudioClip Busy;
    public AudioClip Ringing;
    public AudioClip Idle;
    public AudioClip PhoneDown;

    Inspectable inspectable;
    string initialContText;

    // Use this for initialization
    void Start()
    {
        initialContText = InspectorContinueText.text;
        RecycledVaultScript.TextBlocks.ForEach(block => block.text = RecycledVaultScript.EmptyCharacter.ToString());
        inspectable = GetComponent<Inspectable>();       
        inspectable.AdditionalCommand += () =>
        {
            InspectorContinueText.text = "[Space] To put down phone";
            StartCoroutine(waitForDial());
            CustomCanvas.SetActive(true);
            SoundSource.spatialBlend = 0;
            if (!SoundSource.clip.Equals(Ringing))
            {
                SoundSource.clip = Idle;
                SoundSource.loop = true;
                SoundSource.Play();
                foreach (Transform child in RecycledVaultScript.ButtonsParent)
                    child.GetComponent<Button>().interactable = true;
            }
        };
    }

    // Update is called once per frame
    void Update()
    {
        if (CustomCanvas.activeInHierarchy)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                RecycledVaultScript.Reset();
                StopAllCoroutines();
                if (!SoundSource.clip.Equals(Ringing))
                {
                    SoundSource.clip = PhoneDown;
                    SoundSource.loop = false;
                    SoundSource.Play();
                }
                SoundSource.spatialBlend = 1;
            }
        }
    }

    IEnumerator waitForDial()
    {
        yield return new WaitUntil(() => !RecycledVaultScript.TextBlocks.Exists(block => block.text == RecycledVaultScript.EmptyCharacter.ToString()));
        RecycledVaultScript.Enter();
        if (RecycledVaultScript.UnlockButton.interactable)
        {
            SoundSource.clip = Ringing;
            SoundSource.loop = true;
            SoundSource.Play();
            CallReceiver.loop = true;
            CallReceiver.Play();
            inspectable.InspectorUI.gameObject.SetActive(false);
            CustomCanvas.gameObject.SetActive(false);
            RecycledVaultScript.Reset();
            StopAllCoroutines();
            if (!SoundSource.clip.Equals(Ringing))
            {
                SoundSource.clip = PhoneDown;
                SoundSource.loop = false;
                SoundSource.Play();
            }
            SoundSource.spatialBlend = 1;
        }
        else
        {
            foreach (Transform child in RecycledVaultScript.ButtonsParent)
                child.GetComponent<Button>().interactable = false;
            SoundSource.clip = Busy;
            SoundSource.loop = false;
            SoundSource.Play();
        }
    }
}
